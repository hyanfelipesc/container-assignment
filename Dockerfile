FROM centos:8
RUN yum install httpd -y
WORKDIR /var/www/html
ADD content .
RUN chown -R apache:apache /var/www/html/
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]