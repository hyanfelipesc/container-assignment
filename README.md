# Container-assignment

## Name
Container-assignment Application

## Description
Repository was created in order to deploy the Application of the project 'Assignment' \
This repository should run after the infrastructure project (https://gitlab.com/hyanfelipesc/cf-ecs-assignment)  

2 repositories make up this project the cf-ecs-assignmment (https://gitlab.com/hyanfelipesc/cf-ecs-assignment) and the container-assignment (https://gitlab.com/hyanfelipesc/container-assignment)


## Installation

To deploy the Container you just need to commit on the branch main after that the pipeline is going to start running \
You can also run the pipeline manually

![parameter image](images/Run.png)

This project is using the Credentials and some other informationsas variables, the keys can be checked on the CI/CD settings variables

![Variable image](images/variables.png)

## About the DockerFIle
The Docker file was created to host our web application. \
The content from the website can be found on the folder 'Content' \
After a commit (or manualy run) the pipeline is going to push the image to the ECR created on the infraestructure repository, then the service will be updated and a new content is going to production 

In order to avoid bad deployment on production we set up a manual trigger after the build

![Variable image](images/manualDeploy.png)


## The website
To acess the website, we are using the Load balancer endpoint (so far we dont have a DNS register or a SSL certificate)

![Variable image](images/website.png)

The LoadBalancer endpoint can be found on the AWS console. EC2/LoadBalancer

![Variable image](images/loadbalancer.png)

## The monitoring script
We wrote a script to monitor the HTTP status from the website, the script can be found on this repository (check-status.sh) \
To execute the script Download it, and give it it execution permission ( chmod +x check-status.sh) \
Replace the variable WEBSITE with the correct Load Balancer Endpoint ( it's going to be solved using DNS Register) 

![Variable image](images/checkstatus.png)


Run the script as in the print below:

![Variable image](images/status200.png)
